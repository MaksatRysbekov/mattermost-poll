<?php

namespace PackageVersions;

/**
 * This class is generated by ocramius/package-versions, specifically by
 * @see \PackageVersions\Installer
 *
 * This file is overwritten at every run of `composer install` or `composer update`.
 */
final class Versions
{
    const ROOT_PACKAGE_NAME = 'pixelbrackets/mattermost-poll';
    const VERSIONS = array (
  'doctrine/annotations' => 'v1.6.0@c7f2050c68a9ab0bdb0f98567ec08d80ea7d24d5',
  'doctrine/cache' => 'v1.8.0@d768d58baee9a4862ca783840eca1b9add7a7f57',
  'doctrine/collections' => 'v1.5.0@a01ee38fcd999f34d9bfbcee59dbda5105449cbf',
  'doctrine/common' => 'v2.10.0@30e33f60f64deec87df728c02b107f82cdafad9d',
  'doctrine/dbal' => 'v2.9.2@22800bd651c1d8d2a9719e2a3dc46d5108ebfcc9',
  'doctrine/doctrine-bundle' => '1.10.0@82d2c63cd09acbde2332f55d9aa7b28aefe4983d',
  'doctrine/doctrine-cache-bundle' => '1.3.5@5514c90d9fb595e1095e6d66ebb98ce9ef049927',
  'doctrine/doctrine-migrations-bundle' => 'v1.3.2@49fa399181db4bf4f9f725126bd1cb65c4398dce',
  'doctrine/event-manager' => 'v1.0.0@a520bc093a0170feeb6b14e9d83f3a14452e64b3',
  'doctrine/inflector' => 'v1.3.0@5527a48b7313d15261292c149e55e26eae771b0a',
  'doctrine/instantiator' => '1.1.0@185b8868aa9bf7159f5f953ed5afb2d7fcdc3bda',
  'doctrine/lexer' => 'v1.0.1@83893c552fd2045dd78aef794c31e694c37c0b8c',
  'doctrine/migrations' => 'v1.8.1@215438c0eef3e5f9b7da7d09c6b90756071b43e6',
  'doctrine/orm' => 'v2.6.3@434820973cadf2da2d66e7184be370084cc32ca8',
  'doctrine/persistence' => 'v1.1.0@c0f1c17602afc18b4cbd8e1c8125f264c9cf7d38',
  'doctrine/reflection' => 'v1.0.0@02538d3f95e88eb397a5f86274deb2c6175c2ab6',
  'helhum/dotenv-connector' => 'v2.1.0@056776aaede09afdc05b85d0c86749484ac21957',
  'jdorn/sql-formatter' => 'v1.2.17@64990d96e0959dff8e059dfcdc1af130728d92bc',
  'monolog/monolog' => '1.24.0@bfc9ebb28f97e7a24c45bdc3f0ff482e47bb0266',
  'ocramius/package-versions' => '1.3.0@4489d5002c49d55576fa0ba786f42dbb009be46f',
  'ocramius/proxy-manager' => '2.2.2@14b137b06b0f911944132df9d51e445a35920ab1',
  'psr/cache' => '1.0.1@d11b50ad223250cf17b86e38383413f5a6764bf8',
  'psr/container' => '1.0.0@b7ce3b176482dbbc1245ebf52b181af44c2cf55f',
  'psr/log' => '1.1.0@6c001f1daafa3a3ac1d8ff69ee4db8e799a654dd',
  'psr/simple-cache' => '1.0.1@408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
  'symfony/apache-pack' => 'v1.0.1@3aa5818d73ad2551281fc58a75afd9ca82622e6c',
  'symfony/cache' => 'v3.4.21@86056403bf6cd6f8e65d2b1a3d4958814fe7258e',
  'symfony/class-loader' => 'v3.4.21@4513348012c25148f8cbc3a7761a1d1e60ca3e87',
  'symfony/config' => 'v3.4.21@17c5d8941eb75a03d19bc76a43757738632d87b3',
  'symfony/console' => 'v3.4.21@a700b874d3692bc8342199adfb6d3b99f62cc61a',
  'symfony/debug' => 'v3.4.21@26d7f23b9bd0b93bee5583e4d6ca5cb1ab31b186',
  'symfony/dependency-injection' => 'v3.4.21@928a38b18bd632d67acbca74d0b2eed09915e83e',
  'symfony/doctrine-bridge' => 'v3.4.21@83dc224e8787a063cdd848ad1248ee1f276e4971',
  'symfony/dotenv' => 'v3.4.21@05ec07bd366cd87f01c942303ff27a21de3ae451',
  'symfony/event-dispatcher' => 'v3.4.21@d1cdd46c53c264a2bd42505bd0e8ce21423bd0e2',
  'symfony/filesystem' => 'v3.4.21@c24ce3d18ccc9bb9d7e1d6ce9330fcc6061cafde',
  'symfony/finder' => 'v3.4.21@3f2a2ab6315dd7682d4c16dcae1e7b95c8b8555e',
  'symfony/flex' => 'v1.1.8@955774ecf07b10230bb5b44e150ba078b45f68fa',
  'symfony/framework-bundle' => 'v3.4.21@38042a3bd6a7bcd18e268ae1bf6cf87dd96398b2',
  'symfony/http-foundation' => 'v3.4.21@2b97319e68816d2120eee7f13f4b76da12e04d03',
  'symfony/http-kernel' => 'v3.4.21@60bd9d7444ca436e131c347d78ec039dd99a34b4',
  'symfony/lts' => 'v3@3a4e88df038e3197e6b66d091d2495fd7d255c0b',
  'symfony/monolog-bridge' => 'v3.4.21@0532406c044cd4ba112ccc5f828e4fbf8237343a',
  'symfony/monolog-bundle' => 'v3.3.1@572e143afc03419a75ab002c80a2fd99299195ff',
  'symfony/orm-pack' => 'v1.0.5@1b58f752cd917a08c9c8df020781d9c46a2275b1',
  'symfony/polyfill-apcu' => 'v1.10.0@19e1b73bf255265ad0b568f81766ae2a3266d8d2',
  'symfony/polyfill-ctype' => 'v1.10.0@e3d826245268269cd66f8326bd8bc066687b4a19',
  'symfony/polyfill-mbstring' => 'v1.10.0@c79c051f5b3a46be09205c73b80b346e4153e494',
  'symfony/routing' => 'v3.4.21@445d3629a26930158347a50d1a5f2456c49e0ae6',
  'symfony/yaml' => 'v3.4.21@554a59a1ccbaac238a89b19c8e551a556fd0e2ea',
  'zendframework/zend-code' => '3.3.1@c21db169075c6ec4b342149f446e7b7b724f95eb',
  'zendframework/zend-eventmanager' => '3.2.1@a5e2583a211f73604691586b8406ff7296a946dd',
  'nikic/php-parser' => 'v4.1.1@8aae5b59b83bb4d0dbf07b0a835f2680a658f610',
  'symfony/maker-bundle' => 'v1.11.2@24b19cccad0c658eca516b35b08668d123fabf92',
  'symfony/process' => 'v3.4.21@0d41dd7d95ed179aed6a13393b0f4f97bfa2d25c',
  'symfony/web-server-bundle' => 'v3.4.21@1f0ec2b6823833f800cbec3c233042fe2de9f3d3',
  'symfony/polyfill-iconv' => '*@c5f7f95361bb65b6a7ff326f29ce10ecdf0e8d95',
  'symfony/polyfill-php70' => '*@c5f7f95361bb65b6a7ff326f29ce10ecdf0e8d95',
  'symfony/polyfill-php56' => '*@c5f7f95361bb65b6a7ff326f29ce10ecdf0e8d95',
  'pixelbrackets/mattermost-poll' => 'dev-master@c5f7f95361bb65b6a7ff326f29ce10ecdf0e8d95',
);

    private function __construct()
    {
    }

    /**
     * @throws \OutOfBoundsException if a version cannot be located
     */
    public static function getVersion(string $packageName) : string
    {
        if (isset(self::VERSIONS[$packageName])) {
            return self::VERSIONS[$packageName];
        }

        throw new \OutOfBoundsException(
            'Required package "' . $packageName . '" is not installed: cannot detect its version'
        );
    }
}

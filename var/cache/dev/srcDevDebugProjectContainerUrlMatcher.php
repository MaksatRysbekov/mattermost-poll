<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcDevDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($rawPathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($rawPathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request ?: $this->createRequest($pathinfo);
        $requestMethod = $canonicalMethod = $context->getMethod();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }

        // root
        if ('' === $trimmedPathinfo) {
            $ret = array (  '_controller' => 'App\\Controller\\PollController::index',  '_route' => 'root',);
            if ('/' === substr($pathinfo, -1)) {
                // no-op
            } elseif ('GET' !== $canonicalMethod) {
                goto not_root;
            } else {
                return array_replace($ret, $this->redirect($rawPathinfo.'/', 'root'));
            }

            return $ret;
        }
        not_root:

        // poll-new
        if ('/new' === $pathinfo) {
            $ret = array (  '_controller' => 'App\\Controller\\PollController::new',  '_route' => 'poll-new',);
            if (!in_array($requestMethod, array('POST'))) {
                $allow = array_merge($allow, array('POST'));
                goto not_pollnew;
            }

            return $ret;
        }
        not_pollnew:

        // poll-vote
        if ('/vote' === $pathinfo) {
            $ret = array (  '_controller' => 'App\\Controller\\PollController::vote',  '_route' => 'poll-vote',);
            if (!in_array($requestMethod, array('POST'))) {
                $allow = array_merge($allow, array('POST'));
                goto not_pollvote;
            }

            return $ret;
        }
        not_pollvote:

        // poll-close
        if ('/close' === $pathinfo) {
            $ret = array (  '_controller' => 'App\\Controller\\PollController::close',  '_route' => 'poll-close',);
            if (!in_array($requestMethod, array('POST'))) {
                $allow = array_merge($allow, array('POST'));
                goto not_pollclose;
            }

            return $ret;
        }
        not_pollclose:

        if ('/' === $pathinfo && !$allow) {
            throw new Symfony\Component\Routing\Exception\NoConfigurationException();
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}

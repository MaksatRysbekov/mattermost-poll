<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AnswerRepository")
 */
class Answer
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $answer_id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $title;


    /**
     * @ORM\ManyToMany(targetEntity="Vote")
     * @ORM\JoinTable(name="answer_votes",
     *      joinColumns={@ORM\JoinColumn(name="answerID", referencedColumnName="answer_id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="voteID", referencedColumnName="vote_id", unique=true)}
     * )
     */
    private $votes;

    public function __construct()
    {
        $this->votes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return integer
     */
    public function getAnswerId()
    {
        return $this->answer_id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getVotes()
    {
        return $this->votes->toArray();
    }

    public function addVote(Vote $vote)
    {
        $this->votes[] = $vote;
    }

}
